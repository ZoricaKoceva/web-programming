package mk.ukim.finki.wpaud.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//nekoi binovi koi startuvaat so app, vo nea kje se pishuvat servlet, cim gi instancira tie servleti
//za da se izvrshuva na web, mora da nasleduva od HttpServlet
/*za davanje informacija do kade i kako slusha so dodavanje parametri
     sakame da se mapira na hello
*/

//treba site servleti da se identifikuvaat za kontrolerite da se koristat

@WebServlet(name="helloworldservlet", urlPatterns = "/hello")
public class HelloWorldServlet extends HttpServlet {
    //sakame da handle-ame GET request so method doGet

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // super.doGet(req, resp); constructor-ot po default od samata funkcija

        // za parametri gore vo URL koi se pishuvaat da se zemaat se kako hash: mapa->kluch i vrednost,pr ?username=Zorica
        String username = req.getParameter("username");

        PrintWriter out = resp.getWriter();
        out.write("<html><head></head><body><h1>Hello world "+username+" !</body></html>");
        out.flush();
    }//Handle-a baranja koi se mapiraat na /hello na toa URI
}

//Servlets se za upravuvanje so web baranje